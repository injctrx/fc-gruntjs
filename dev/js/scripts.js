var app = app || {};


app.init = function () {

  app.sample();

}


app.sample = function () {

  var container = $('#element');

  var mod = this;

  mod.init = function() {}

  mod.foo = function() {}

  mod.bar = function() {}

  if (container.length) mod.init();

}


$(function() {

  app.init();

});