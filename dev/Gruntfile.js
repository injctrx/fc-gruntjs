'use strict';

module.exports = function(grunt) {

  grunt.initConfig({

    pkg: grunt.file.readJSON('package.json'),

    conf: {
      src  : '',
      dest : '../',
    },

    ect: {
      files: {
        expand: true,
        cwd: '<%= conf.src %>ect',
        src: ['**/*.ect', '!_layouts/**', '!_parts/**'],
        dest: '<%= conf.dest %>',
        ext: '.html'
      },
      options: {
        pretty: true
      }
    },

    pug: {
      files: {
        expand: true,
        cwd: '<%= conf.src %>pug',
        src: ['**/*.pug', '!_layouts/**', '!_parts/**', '!_mixins/**'],
        dest: '<%= conf.dest %>',
        ext: '.html'
      },
      options: {
        pretty: true
      }
    },

    compass: {
      scss: {
        options: {
          sourcemap: true,
          noLineComments: true,
          sassDir: '<%= conf.src %>scss',
          cssDir: '<%= conf.dest %>css'
        }
      }
    },

    autoprefixer: {
      options: {
        browsers: ['last 2 version', 'ie 9']
      },
      multiple_files: {
        expand: true,
        flatten: true,
        cwd: '<%= conf.dest %>css',
        src: ['*.css', '!*.min.css'],
        dest: '<%= conf.dest %>css'
      }
    },

    concat: {
      libs: {
        src: '<%= conf.src %>js/libs/*.js',
        dest: '<%= conf.dest %>js/libs.js'
      },
      scripts: {
        files: [{
          expand: true,
          cwd: '<%= conf.src %>js',
          src: ['**/*.*', '!libs/**', '!helpers/**'],
          dest: '<%= conf.dest %>js'
        }]
      }
    },

    imagemin: {
      filesdynamic: {
        files: [{
          expand: true,
          cwd: '<%= conf.src %>img',
          src: ['**/*.{png,jpg,gif}'],
          dest: '<%= conf.dest %>img'
        }]
      }
    },

    prettify: {
      options: {
        indent: 2,
        indent_char: ' ',
        wrap_line_length: 78,
        brace_style: 'expand',
        unformatted: ['pre', 'code'],
        indent_inner_html: false
      },
      files: {
        expand: true,
        cwd: '<%= conf.dest %>',
        ext: '.html',
        src: ['**/*.html', '!dev/**'],
        dest: '<%= conf.dest %>'
      }
    },

    watch: {
      build_js: {
        files: ['<%= conf.src %>js/**/*.js'],
        tasks: ['concat']
      },
      build_css: {
        files: ['<%= conf.src %>scss/**/*.scss'],
        tasks: ['compass']
      },
      build_image: {
        files: ['<%= conf.src %>img/**/*.{png,jpg,gif}'],
        tasks: ['imagemin-newer']
      },
      build_ect: {
        files: ['<%= conf.src %>ect/**/*.ect'],
        tasks: ['ect', 'prettify']
      },
      //build_pug: {
      //  files: ['<%= conf.src %>pug/**/*.pug'],
      //  tasks: ['pug']
      //},
    }

  });


  grunt.loadNpmTasks('grunt-contrib-imagemin');
  grunt.loadNpmTasks('grunt-contrib-compass');
  grunt.loadNpmTasks('grunt-contrib-concat');
  //grunt.loadNpmTasks('grunt-contrib-pug');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-ect');
  grunt.loadNpmTasks('grunt-newer');
  grunt.loadNpmTasks('grunt-prettify');


  grunt.registerTask('default', [
    'compass',
    'concat',
    'ect',
    'prettify',
    //'pug',
    'imagemin',
    'watch'
  ]);

  grunt.registerTask('imagemin-newer', ['newer:imagemin:filesdynamic']);

}